#!/usr/bin/make -f

DEB_BUILD_MAINT_OPTIONS = hardening=+all

# This defines and exports DEB_HOST_MULTIARCH and a host of other garbage
include /usr/share/dpkg/default.mk

# Manually implement multiarch support here since debhelper seems to pass
# this information in a proper form only to the Autoconf build system.
# When using "--buildsystem=make", no multiarch support is added.
# Chibi does not support Autoconf so we have to fix up the paths here.
EXTRA_ARGS += prefix="/usr"
EXTRA_ARGS += libdir="/usr/lib/$(DEB_HOST_MULTIARCH)"
EXTRA_ARGS += SNOWPREFIX="/usr/local"
EXTRA_ARGS += SNOWLIBDIR="/usr/local/lib/$(DEB_HOST_MULTIARCH)"

%:
	dh $@

override_dh_auto_build:
# Build the static library first. It requires special compilation flags without
# -fPIC and dlopen() support.
	dh_auto_build -- libchibi-scheme.a $(EXTRA_ARGS) CLIBFLAGS= SEXP_USE_DL=0
	$(MAKE) clean
# After cleaning up the object files we can build everything else.
	dh_auto_build -- all $(EXTRA_ARGS)
# Additionally, build the HTML documentation for Chibi and libraries.
	$(MAKE) doc

# Run a bit more tests than "make test" usually does to verify
# SRFI libraries and FFI things.
override_dh_auto_test:
	dh_auto_test -- test-all test-unicode $(EXTRA_ARGS)

override_dh_auto_install:
# Make sure that "make install" does not build and install image files.
# Those will be created on the user machine at package configuration time.
	dh_auto_install -- $(EXTRA_ARGS) IMAGE_FILES=
